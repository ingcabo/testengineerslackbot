module.exports = {
    apps : [{
      name        : "worker-SlackBot",
      script      : "./slack-client/index-server-bot.js",
      watch       : true,
      //max_memory_restart: "9000M",
      env: {
        "NODE_ENV": "development",
      },
      env_production : {
         "NODE_ENV": "production"
      }
    }]
  }