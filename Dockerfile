FROM node:12.20.1-alpine3.12

ENV SLACK_BOT_PORT='3000'
ENV POSTMAN_API_KEY=''
ENV AUTH_WEB_APP=''
ENV API_KEY_WEB_APP=''
ENV WEBHOOK_URL_PATH=''
ENV POSTMAN_API_URL=https://api.getpostman.com/
ENV URL_WEB_APP=http://automation-service-postman:3004/
ENV STATIC_REPORT_URL=''
ENV CHANNEL_SLACK_BOT=''
ENV NAME_SLACK_BOT=''
ENV TOKEN_SLACK_BOT=''
ENV SLACK_SIGNING_SECRET=''
ENV SLACK_BOT_TOKEN=''
ENV NODE_TLS_REJECT_UNAUTHORIZED=0

RUN apk --no-cache add procps=3.3.16-r0 curl jq=1.6-r1 bash=5.0.17-r0 && npm install pm2@^4.4.1 -g && mkdir /etc/botserver

WORKDIR /etc/botserver

COPY  package.json /etc/botserver/package.json
COPY  package-lock.json /etc/botserver/package-lock.json
COPY  actions/ /etc/botserver/actions
COPY  clients/ /etc/botserver/clients
COPY  resources/ /etc/botserver/resources
COPY  slack-client/ /etc/botserver/slack-client
COPY  ecosystem.config.js /etc/botserver/ecosystem.config.js

RUN npm ci

WORKDIR /etc/botserver

CMD ["pm2-runtime", "start", "ecosystem.config.js"]