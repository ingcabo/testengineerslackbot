
const ShowHelpAcion = require('../actions/ShowHelpAcion.js');
const GetJokesAction = require('../actions/GetJokesAction.js');
const GetListTestsAction = require('../actions/GetListTestsAction.js');
const GetTestAction =require('../actions/GetTestAction.js')
const GetExecutionTestAction = require('../actions/GetExecutionTestAction.js');
const PostAddTestAction = require('../actions/PostAddTestAction.js');
const DeleteTestAction = require('../actions/DeleteTestAction.js');

    async function handleMessage(COMMANDS,data) {
        let message = data.toLowerCase();
        switch (true) {
            case message.includes(COMMANDS.JOKE.command):
                return await GetJokesAction();
            case message.includes(COMMANDS.HELP.command):
                return ShowHelpAcion();
            case message.includes(COMMANDS.EXECUTE.command):
                return await GetExecutionTestAction.perform(message);
            case message.includes(COMMANDS.REQUEST_SET.command):
                return await GetTestAction.perform(message);
            case message.includes(COMMANDS.GET_TESTS.command):
                return await GetListTestsAction.perform();
            case message.trim().includes(COMMANDS.ADD_TEST.command):
                return await PostAddTestAction.perform(data)
            case message.trim().includes(COMMANDS.DELETE_SET.command):
                return await DeleteTestAction.perform(message)
            default:
                console.log('there are not action for this option', message);
                return `howdy`
        }
    };

    module.exports = handleMessage;