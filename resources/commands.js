const COMMANDS = {
    GET_TESTS: {
        'command': "--tests list",
        'description': '[list the tests]'
    },
    REQUEST_SET: {
        'command': "--consult set",
        'description': '[consult set]'
    },
    EXECUTE: {
        'command': "--execute",
        'description': '[execute]'
    },
    DELETE_SET: {
        'command': "--delete set",
        'description': '[remove set]'
    },
    JOKE: {
        'command': "--tell a joke",
        'description': '[tell a joke]'
    },
    HELP: {
        'command': "--help",
        'description': '[help]'
    },
    ADD_TEST: {
        'command': '"bail":',
        'description': '[add test]'
    }
}


const GET_TESTS = 'GET_TESTS';
const REQUEST_SET = 'REQUEST_SET';
const DELETE_SET = 'DELETE_SET';
const JOKE = 'JOKE';
const HELP = 'HELP';


module.exports = COMMANDS;