const fetch = require("isomorphic-unfetch");

class BaseClient {

    constructor(baseUrl){
        this.baseUrl=baseUrl;
    }

    async request(endpoint="",options={}){
        let url = this.baseUrl + endpoint;

        console.log("url===",url)
     
        let config = {
            ...options,
        };

        return fetch(url,config).then(r=>{
            if (r.ok){
                return r.json()
            }
            throw new Error(r);
        })
    }


}

module.exports = BaseClient