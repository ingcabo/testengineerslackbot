const BaseClient = require('./BaseClient.js');

class AutomationServicesClient extends BaseClient {
    
    constructor(){
        let host   = process.env.URL_WEB_APP;
        let token = process.env.AUTH_WEB_APP;
        if (host == undefined || token == undefined){
            throw 'The host and token are required'
        }

        super(host)
        this.headers = {
            'Authorization': token,
            "Content-type":"application/json"
        };
    }
    
    async performGet(path,errorMessage,params={}){


      console.log("performGet ",path)
        const options = {
            method:"GET",
            headers:this.headers,
            params
        };
        console.log("options ",JSON.stringify(options))
        try{
            let response = await this.request(path,options);
            return response['data'];
        }catch(error){
            console.error(error);
            return errorMessage;
        }
    }


    async performPost(path,object,errorMessage){

        const options = {
            method:"POST",
            headers:this.headers,
            body: JSON.stringify(object)
        };
        try{
        let response = await this.request(path,options);
        return response['data'];
        }catch(error){
            console.error(error);
        }
    }

    async performDelete(path,errorMessage){
        
        const options = {
            method:"DELETE",
            headers:this.headers,
        };

        try{
            let response = await this.request(path,options)
            return response['data'];
        }catch{
            console.error(error);
            return errorMessage;
        }
        
    }


}

module.exports = new AutomationServicesClient();