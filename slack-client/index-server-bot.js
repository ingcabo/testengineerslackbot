const { App,directMention } = require('@slack/bolt');
const  COMMANDS  = require('../resources/commands.js')
const handleMessage = require('../resources/selectCaseAction.js')

    const app = new App({
        signingSecret: process.env.SLACK_SIGNING_SECRET,
        token: process.env.SLACK_BOT_TOKEN,
    });
  
        
      (async  () => {
        //Start up the app
        const server = await app.start(process.env.SLACK_BOR_PORT || 3000);
        console.log('⚡️. Slack Bot app is running!', server.address());
      })();

      app.message(async ({ message, say }) => {
        let data = message.text;
        let response = await handleMessage(COMMANDS,data);
        if (response !='howdy'){
          await say(response);
        }

      });