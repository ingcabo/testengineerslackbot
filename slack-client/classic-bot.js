'use strict';
const SlackBot = require('slackbots');
const  COMMANDS  = require('../resources/commands.js')
const handleMessage = require('../resources/selectCaseAction.js')

const URL_WEB_APP = process.env.URL_WEB_APP;
const AUTH_WEB_APP = process.env.AUTH_WEB_APP;
const API_KEY_WEB_APP = process.env.API_KEY_WEB_APP;

class myBotx  extends SlackBot{

    constructor(params) {
        super(params);
        this.token = params.token;
        this.name = params.name;
        this.disconnect = false;
        this.channel= params.channel;
    }

    slackbt() {
       
        return new Promise((resolve, reject) => {
            // Start Handler
            this.on('start', () => {
            });
       
            // Error Handler
            this.on('error', err => console.log(err));
       
            // Message Handler
            this.on('message',  data => {
                
                if (data.type !== 'message') {

                    return;
                }
               console.log("data.text ",data)
                let response =  handleMessage(COMMANDS,data.text);
                if (response !='howdy'){
                   //say(response);
                }
            });
            resolve(this.bot);
        });
    }
}

module.exports = myBotx;