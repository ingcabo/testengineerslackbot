#  Slack bot service for QA!

the **slack bot** client allows us to integrate into executions of **postman collections** using slack messaging.

### Starting 🚀
These instructions will allow you to get a copy of the project running on your local machine for development and testing purposes.

See Deployment to know how to deploy the project.

### Prerequisites 📋
What things do you need to install the software and how to install them.

- [Git Bash](https://www.npmjs.com/get-npm).
- [Node](https://nodejs.org/en/download/).
- [npm](https://www.npmjs.com/get-npm).
- [nodemon](https://www.npmjs.com/package/nodemon).

What things do you need to configure the software.


-[A basic App for Slack](https://api.slack.com/apps).<br/>
-Bot Name Slack.<br/>
-Bot User OAuth Token.<br/>
-Slack Channel name  and Webhook URLs for Slack Channel (invite your bot to the channel).<br/>

What things do you need to connect the software with the [automation_service_postman](https://github.com/...).

- Url
- Port
- Authorization header token automation service postman - > Authorization:{{basic_auth}}

which are the environment variables that I need to initialize.
```
 CHANNEL_SLACK_BOT="Slack Channel name".<br/>
 NAME_SLACK_BOT="Bot Name Slack".<br/>
 TOKEN_SLACK_BOT=" Bot User OAuth Token" .<br/>
 AUTH_WEB_APP="Authorization header token automation service postman" .<br/>
 URL_WEB_APP="UrlService:PortService".<br/>
 WEBHOOK_URL_PATH="Slack WEBHOOK path url"
 STATIC_REPORT_URL= "http url report address"
 ```
 
## run the aplication dev

```nodemon index_bot.js```

##  Create Docker image

sudo docker build -t bot_service:slack_bot .

##  Create Docker container

sudo docker run -it  -d --name bot_service_local  --env-file ./env.list  bot_service:slack_bot 

or Docker Compose

- you have to have both images available [bot_service:slack_bot, automation_service:service_postman]


docker-compose up -d slack_bot postman_service --env-file ./env.list

## UML diagrams

```
sequenceDiagram
    participant bot_service
    participant automation_service_postman
    participant POSTMAN_API
    
bot_service->>automation_service_postman: URL_WEB_APP
automation_service_postman-->>POSTMAN_API: POSTMAN_API_URL
automation_service_postman-->bot_service: CRUD

 ```

# commands

to interact with the slack bot there are a couple of commands.

#### Create Set
to save a test configuration, you need to send this json to the slack bod channel

```json

{  
"NameTest": 
	[  
			{  
			"name": "ReportName",  
			"environmentName": "", 
			"iterationData":"", 
			"iterationCount":"",
			"collectionName": "",
			"slack":"",  
			"bail": "false"  
			}  
	]  
}
```

|       field    |  Description                  |             required        |
|----------------|-------------------------------|-----------------------------|
|NameTest        | Test name                     |yes                          |
|name            | Report Name                   |yes                          |
|environmentName | Postman Environment Variable  |yes                          |
|iterationData   | the JSON or CSV file to be used as data source  |no  |
|iterationCount  | number of iterations to run on the collection  |no  |
|collectionName | Postman Collection Name  |yes                          |
|slack | Webhook URLs for Slack Channel  |yes                          |
|bail | to specify whether or not to gracefully stop a collection run  |yes                          |

#### --tests list

we can check what is available to execute
>--tests list
output example
>[0] CoreAPI[1] APP+AppSmoke
>[2] CoreCommunityCases
>[3] CoreCommunityCases

#### --consult set
we can query a particular test set using the index number
>--consult set 0
output example
[{"name":"APP+AppSmoke","environmentName":"APP+AppSmoke","collectionName":"APP+AppSmoke","bail":"false"}]


#### --execute
to run a test

>--execute 0
output example
your test is running...

Success Test APP+AppSmoke   6/10/2021, 3:59:43 PM  
  • iterations          • requests  
  executed: 1          executed: 48  
  failed: 0                failed: 0  
  • testScripts      • prerequestScripts  
  executed: 96          executed: 93  
  failed: 0                failed: 0  
  • assertions        • timings  
  executed: 102          average resp time:1697.499ms  
  failed: 0                 run duration:83288ms     
  [Click here to view the html report](http://localhost:3004/app/index.html)  
    
• errordetail  
  []

#### --delete set
to delete a test
>--delete set 0
output example
Set Removed!!
