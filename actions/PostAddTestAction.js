
const automationClient = require('../clients/AutomationService');
const PATH_SET_TEST ='/settest'

class PostAddTestAction{

    async perform(message){

        let objectToSave = this.JsonValidation(message);

        if (!objectToSave){
            return "it is not a valid json"
        }else{

            let addTest = await automationClient.performPost(PATH_SET_TEST,objectToSave,"");
            console.log('addTest',addTest);
            return addTest;
        }
        
    }

    
    JsonValidation(objString){
        let NewobjString = objString.trim();
        NewobjString = NewobjString.replace(/(\r\n|\n|\r)/gm, "").replace(/\s/g, '').trim();
        try {
            JSON.parse(NewobjString);
            if (NewobjString.substring(0, 2) == '{"' && NewobjString.substring(NewobjString.length - 4) == '"}]}') {
                let myArr = JSON.parse(NewobjString);
                return myArr;
            } else {
                return false;
            }
        } catch (e) {
            console.log(e);
            return false;
        }

    }


}

module.exports = new PostAddTestAction();

