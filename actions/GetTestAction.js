const automationClient = require('../clients/AutomationService');
//const GetListTestsAction = require('../resources/selectCaseAction.js');
const  COMMANDS  = require('../resources/commands.js')
const ToolActions = require('./ToolsActions.js')

let GET_TEST = "/settest";
const LIST_TESTS = "/settest";
const commandLength = COMMANDS.REQUEST_SET.command.length;

class GetTestAction  {
    
 
    async perform(message){
        let testList = await automationClient.performGet(LIST_TESTS,"");
        let testListKey = ToolActions.objectsAsKey(testList);
        let testName=  ToolActions.getIndexTestName(ToolActions.extrctParam(message, commandLength),testListKey);
        let testsInformations = await automationClient.performGet(`${GET_TEST}?testname=${testName}`,"");
        testsInformations = JSON.stringify(testsInformations);
        return testsInformations
      
    }


}

module.exports = new GetTestAction();



