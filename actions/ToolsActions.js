
class ToolActions{

    getIndexTestName(num,testListKey) {
        let name = "";
        if (testListKey[num]) {
            name = testListKey[num];
        } else {
            name = "invalid!!";
        }
        return name;
    }

     extrctParam(text, index = 8) {
        var str = text.trim();
        var res = parseInt(str.substring(index, str.length));
        return res;
    }

    objectsAsKey(arrayList){
        return Object.keys(arrayList)
    }
}

module.exports = new ToolActions();