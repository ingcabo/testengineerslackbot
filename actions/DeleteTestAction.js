const automationClient = require('../clients/AutomationService');
const  COMMANDS  = require('../resources/commands.js')
const ToolActions = require('./ToolsActions.js')

let DELETE_TEST = "/settest";
const LIST_TESTS = "/settest";
const commandLength = COMMANDS.DELETE_SET.command.length;

class DeleteTestAction  {
    
 
    async perform(message){
        let testList = await automationClient.performGet(LIST_TESTS,"");
        let testListKey = ToolActions.objectsAsKey(testList);
        let testName=  ToolActions.getIndexTestName(ToolActions.extrctParam(message, commandLength),testListKey);
        let response = await automationClient.performDelete(`${DELETE_TEST}/${testName}`,"");
        response = JSON.stringify(response);
        return response
    }


}

module.exports = new DeleteTestAction();