const automationClient = require('../clients/AutomationService.js');

const JOKES_PATH = '/jokes';

async function GetJokesAction(){
    let response = await automationClient.performGet(JOKES_PATH,"I can't get the joke");
    console.log('GetJokesAction', JSON.stringify(response))
    return response
}

module.exports = GetJokesAction;