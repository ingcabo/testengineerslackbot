const automationClient = require('../clients/AutomationService');
const GetListTestsAction = require('./GetListTestsAction.js');
const  COMMANDS  = require('../resources/commands.js')
const ToolActions = require('./ToolsActions.js')

let EXECUTE_TEST = "/getexecute";
const LIST_TESTS = "/settest";
const commandLength = COMMANDS.EXECUTE.command.length;

class GetExecutionTestAction  {
    
 
    async perform(message){
        let testList = await automationClient.performGet(LIST_TESTS,"");
        let testListKey = ToolActions.objectsAsKey(testList);
        let testName=  ToolActions.getIndexTestName(ToolActions.extrctParam(message, commandLength),testListKey);
        let testsInformations =  automationClient.performGet(`${EXECUTE_TEST}?test=${testName}`,"");
        return  "your test is running...";
      
    }


}

module.exports = new GetExecutionTestAction();



