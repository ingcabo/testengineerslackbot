const automationClient = require('../clients/AutomationService');

const LIST_TESTS = "/settest";

class GetListTests{

    async perform(){
        let tests = await automationClient.performGet(LIST_TESTS,"");
        let keysNames = [];
        keysNames = Object.keys(tests);
        return keysNames.length > 0 ? this.prettyFormatForTests(keysNames) :"Could not find tests to run"; 
    }

    prettyFormatForTests(keysNames){
        let text = "";
        if (keysNames.length > 0) {
            text += "We can run...  \n\n"
            for (var i = 0; i < keysNames.length; i++) {
                text += `[${i}] ${keysNames[i]} \n\n`;
            }
        }
        return text;
    }
}

module.exports = new GetListTests();